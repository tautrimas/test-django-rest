# Why?

Testing out Django REST Framework.

## Installation

```sh
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python manage.py test
```

Sample output:

```
$ ./manage.py test
Creating test database for alias 'default'...
System check identified no issues (0 silenced).
..
----------------------------------------------------------------------
Ran 2 tests in 0.259s

OK
Destroying test database for alias 'default'...
```
