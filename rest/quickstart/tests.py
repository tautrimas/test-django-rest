from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase, APIRequestFactory
from rest_framework.test import force_authenticate
from rest_framework.utils import json

from rest.quickstart.views import UserViewSet


class UserTests(APITestCase):
    def test_users_list(self):
        """
        Having some users, ensure user list API returns them.
        """
        factory = APIRequestFactory()
        admin_user = User.objects.create_superuser('admin', 'admin@example.com', password='test')
        # noinspection PyCallByClass,PyTypeChecker
        view = UserViewSet.as_view({'get': 'list'})

        request = factory.get('/users/')
        force_authenticate(request, admin_user)

        response = view(request)
        response.render()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), [{
            'url': 'http://testserver/users/1/',
            'username': 'admin',
            'email': 'admin@example.com',
            'groups': []
        }])

    def test_user_create(self):
        """
        Ensure user can create user.
        """
        factory = APIRequestFactory()
        admin_user = User.objects.create_superuser('admin', 'admin@example.com', password='test')
        # noinspection PyCallByClass,PyTypeChecker
        view = UserViewSet.as_view({'post': 'create'})

        data = {
            'username': 'test_name',
            'email': 'user@example.com',
            'groups': [],
        }
        request = factory.post('/users/', data)
        force_authenticate(request, admin_user)

        response = view(request)
        response.render()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.content)
